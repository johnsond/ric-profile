#!/bin/sh

set -x

ALLNODESCRIPTS="setup-ssh.sh"
RICSCRIPTS="setup-ric.sh"
EPCSCRIPTS="setup-epc.sh"
RANSCRIPTS="setup-ran.sh"

export SRC=`dirname $0`
cd $SRC
. $SRC/setup-lib.sh

# Don't run setup-driver.sh twice
if [ -f $OURDIR/setup-driver-done ]; then
    echo "setup-driver already ran; not running again"
    exit 0
fi

for script in $ALLNODESCRIPTS ; do
    cd $SRC
    sname=`echo $script | cut -d. -f1`
    $SRC/$script | tee - $OURDIR/$sname.log 2>&1
done

if [ $NODEID = "ric" ]; then
    for script in $RICSCRIPTS ; do
	cd $SRC
	sname=`echo $script | cut -d. -f1`
	$SRC/$script | tee - $OURDIR/$sname.log 2>&1
    done
elif [ $NODEID = "epc" ]; then
    for script in $EPCSCRIPTS ; do
	cd $SRC
	sname=`echo $script | cut -d. -f1`
	$SRC/$script | tee - $OURDIR/$sname.log 2>&1
    done
elif [ $NODEID = "ran" ]; then
    for script in $RANSCRIPTS ; do
	cd $SRC
	sname=`echo $script | cut -d. -f1`
	$SRC/$script | tee - $OURDIR/$sname.log 2>&1
    done
fi

exit 0
