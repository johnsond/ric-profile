# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the InstaGENI library.
import geni.rspec.igext as ig
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a portal object,
pc = portal.Context()

pc.defineParameter(
    "ricBranch","RIC E2 Component Branch",
    portal.ParameterType.STRING,
    'master',[('master','master'),('bronze','bronze'),
              ('r2','r2 (unsupported)'),('Amber','Amber (unsupported)')],
    longDescription="Which branch/release you want to use for the e2term and e2mgr components.  Other components are pulled from the master branch.  If you use the master branch for the E2 components, things may be unstable.")
pc.defineParameter(
    "type","Hardware Type",portal.ParameterType.NODETYPE,"d740",
    longDescription="Hardware type, if physical node or dedicated VM.")
pc.defineParameter(
    "vm","Virtual Machine",portal.ParameterType.STRING,"none",
    [("none","none"),("dedicated","dedicated"),("shared","shared")],
    longDescription="Whether to use a dedicated or shared VM, or none at all.")
pc.defineParameter(
    "image","Node Image",portal.ParameterType.IMAGE,
    "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD",
    longDescription="The image your nodes will run.")
pc.defineParameter(
    "doRAN","Add NB/UE node",portal.ParameterType.BOOLEAN,True,
    longDescription="Add a node for simulated NB/UE.")
pc.defineParameter(
    "doEPC","Add EPC node",portal.ParameterType.BOOLEAN,False,
    longDescription="Add a node EPC.")
pc.defineParameter(
    "linkSpeed","LAN/Link Speed",portal.ParameterType.INTEGER,0,
    [(0,"Any"),(100000,"100Mb/s"),(1000000,"1Gb/s"),(10000000,"10Gb/s"),
     (25000000,"25Gb/s"),(100000000,"100Gb/s")],
    longDescription="A specific link speed to use for each LAN and link.",
    advanced=True)
pc.defineParameter(
    "linkDelay","LAN/Link Delay",portal.ParameterType.INTEGER,0,
    longDescription="A specific link delay (ms) to use for each LAN and link.",
    advanced=True)
pc.defineParameter(
    "multiplex","Multiplex Networks",portal.ParameterType.BOOLEAN,False,
    longDescription="Multiplex all LANs and links.",
    advanced=True)
pc.defineParameter(
    "bestEffort","Best Effort Link Bandwidth",portal.ParameterType.BOOLEAN,False,
    longDescription="Do not require guaranteed bandwidth throughout switch fabric.",
    advanced=True)
pc.defineParameter(
    "vlanTagging","VLAN Tagging",portal.ParameterType.BOOLEAN,False,
    longDescription="Expose VLAN tags to experiment nodes (by default experiment link ports untag host-bound traffic.",
    advanced=True)
pc.defineParameter(
    "forceShaping","Force Traffic Shaping",portal.ParameterType.BOOLEAN,False,
    longDescription="Force traffic shaping, even if none is deemed necessary.",
    advanced=True)

params = pc.bindParameters()

#
# Sometimes we might want to disable the testbed's root ssh key service.
# It seems to race (rarely) with our startup scripts.  However, we only
# want to do this when we're running setup-ssh.sh as root, which we
# don't do in this profile.  This profile runs `sudo` as needed, so we
# want all nodes accessible via the user.
#
disableTestbedRootKeys = False

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

tour = ig.Tour()
tour.Description(
    ig.Tour.TEXT,"Instantiate a demo of the O-RAN RIC E2 components.")
tourInstructions = \
  """
## Instructions
This profile creates a LAN of three nodes: one to host RIC O-RAN components; another to host OpenAirInterface (OAI) and srsLTE eNodeBs and UEs; and another to host an LTE EPC (NextEPC).  This environment serves as a demonstration and development environment for [our OAI O-RAN/RIC agent](https://gitlab.flux.utah.edu/powderrenewpublic/oai-ric), and [our srsLTE O-RAN/RIC agent](https://gitlab.flux.utah.edu/powderrenewpublic/srslte-ric).  This profile's [git repository](https://gitlab.flux.utah.edu/johnsond/ric-profile) contains several shell scripts (`setup-{ric|ran|epc}.sh`) that each clone source code repositories for these components, and build/install/configure them to talk to one another.

For each set of components, we describe how the profile configures its resources, and provide instructions to run simple demos.

### O-RAN/RIC

First, `setup-ric.sh` configures the `ric` bare metal node.  It clones O-RAN RIC E2 component repositories; builds Docker images of them; and spawns several Docker containers configured to talk to each other on a bridged Docker network.  It will do this in your home directory, and log its progress to `~/setup.log`, so you can see its status and progress.  (This Docker network is bridged to the rest of the physical LAN, so RIC containers can communicate with OAI/srsLTE RAN elements running elsewhere, and/or EPC elements.  You can launch more containers on this network, but specify an IP in the 10.0.2.0/24 network above 10.0.2.100 so that you don't interfere with other pre-configured IP endpoints.)

### O-RAN/RIC Demo Setup

Once setup scripts on the `ric` node have finished (~15-25 minutes, depending on the hardware type of your bare-metal nodes, and other factors like apt mirror speed), you can follow these instructions to try out the O-RAN RIC E2 components against OAI and/or srsLTE.  (If you select the old, unmainted r2 or Amber branches, you might be able to run the e2sim and OAI demos, but not the srsLTE part.)  Here are instructions to get set to run the demos.  (If you want to restart the E2 containers from scratch, just run `sudo rm -f /local/setup/setup-ric-done && sudo /local/repository/setup-ric.sh`; this will stop and remove any running RIC containers, and start them up again, so you are ready to move through the instructions again.)

1. Ensure that RIC setup has finished.  Open an ssh connection to your `ric` node, and
```
tail -F ~/setup.log
```
If you see continuing output, setup is still working.  You should see an `+ exit 0` at the bottom.  If any of the commands in Step 2 below fail, either setup is still running, or underlying container image builds have failed.

2. Open four ssh connections to your `ric` node.  Run the following commands in the first four shells:
```
docker logs -f e2term
docker logs -f e2mgr
docker logs -f db
```
(These commands allow you to view the stdout/stderr of PID 1 in the container; e.g., the e2 components or the redis db.  The 4th shell will be for manually typing API requests to the e2mgr via curl.  The `e2term` component is a "termination" point for E2AP connections to RAN nodes; the `e2mgr` component provides management services for connected RAN nodes.)

3. Spawn a temporary demo container attached to the `ric` network in your 5th shell:
```
docker run --rm -it --network ric --name demo --ip 10.0.2.101 ric-demo:latest /bin/bash
```

### OpenAirInterface (OAI) with RIC

The `setup-ran.sh` shell script builds and configures simulated OAI eNodeB and UE binaries on the `ran` node.  It also generates an eNodeB config file that talks to the EPC services on the `epc` node.

(We don't use UEs in this demo at the moment, so these instructions only mention a single, simulated eNodeB.  Eventually, of course, the profile will be enhanced to run N eNodeBs and M UEs, either in simulated mode or in hardware mode at POWDER USRPs.)

This demo shows how to connect an OAI eNodeB with O-RAN RIC E2 support to a RIC controller, and to run simple `e2mgr` management API calls against it.

1. Run your simulated OAI eNodeB:
```
sudo RFSIMULATOR=enb /local/setup/oai-ric/cmake_targets/ran_build/build/lte-softmodem -O /local/setup/enb.conf --rfsim
```
(Note that by default, /local/setup/enb.conf is a symlink to a band 48 US CBRS TDD eNodeB config; there is also an FDD band 4 AWS eNodeB config, but that will not work the existing nascent gNB X2/E2 configuration.)

2. In the demo container, you can run these `curl` API invocations to exercise the e2mgr component's RESTful API.  For instance:

  - List the connected RAN nodes:
```
curl -X GET http://e2mgr:3800/v1/nodeb/ids
```

(Grab the value of the `inventoryName` key, which should look something like `enB-macro:661-8112-0019b0`.)

  - Show details of the `enB-macro:661-8112-0019b0` RAN node:
```
curl -X GET http://e2mgr:3800/v1/nodeb/enB-macro:661-8112-0019b0
```
  - Tell the e2mgr to shutdown the nodeb (removes e2 state for the RAN node, and breaks the connection to the RAN node).
```
curl -X PUT http://e2mgr:3800/v1/nodeb/shutdown
```

### srsLTE with RIC

The `setup-ran.sh` shell script builds and configures simulated srsLTE `srsenb` binaries on the `ran` node.  This demo shows how to connect an srsLTE eNodeB with O-RAN RIC E2 support to a RIC controller, and to run simple `e2mgr` management API calls against it.

1. Run your simulated srsLTE eNodeB:
```
/local/setup/srslte-ric/build/srsenb/src/srsenb --enb.name=enb1 --enb.enb_id=0x19B --rf.device_name=zmq --rf.device_args=fail_on_disconnect=true,id=enb,base_srate=23.04e6,tx_port=tcp://*:2000,rx_port=tcp://localhost:2001 --ric.agent.remote_ipv4_addr=10.0.2.10 --log.all_level=info --ric.agent.log_level=debug --log.filename=stdout
```

2.  Return to the demo Docker container, and re-run commands from Step 2 of the OAI section above (noting that the eNodeB `inventoryName` value will have changed).
"""
tour.Instructions(ig.Tour.MARKDOWN,tourInstructions)
request.addTour(tour)

def setLanLinkParams(lanlink):
    if params.bestEffort:
        lanlink.best_effort = True
    if params.multiplex:
        lanlink.link_multiplexing = True
    if params.vlanTagging:
        lanlink.vlan_tagging = True
    if params.linkSpeed > 0:
        lanlink.bandwidth = params.linkSpeed
    if params.linkDelay > 0:
        lanlink.latency = params.linkDelay
    if params.forceShaping:
        lanlink.setForceShaping()

nodemap = dict(ric="10.0.2.1")
lan = None
netmask = "255.255.255.0"
if params.doRAN:
    nodemap["ran"] = "10.0.2.2"
if params.doEPC:
    nodemap["epc"] = "10.0.2.3"
if params.doRAN or params.doEPC:
    lan = pg.LAN("riclan")
    setLanLinkParams(lan)

for (name,ip) in nodemap.items():
    if params.vm == "none":
        node = pg.RawPC(name)
        if disableTestbedRootKeys:
            node.installRootKeys(False,False)
        if params.type:
            node.hardware_type = params.type
    elif params.vm == "dedicated":
        vhost = pg.RawPC("vhost")
        vhost.exclusive = True
        if params.type:
            vhost.hardware_type = params.type
        node = ig.XenVM(name)
        node.exclusive = True
        request.addResource(vhost)
    else:
        node = ig.XenVM(name)
        node.exclusive = False
    if params.image:
        node.disk_image = params.image
    cmd = "sudo -u `cat /var/emulab/boot/swapper` -Hi /bin/sh -c '/local/repository/setup.sh >setup.log 2>&1'"
    node.addService(pg.Execute(shell="sh",command=cmd))
    if lan:
        iface = node.addInterface("ifr")
        iface.addAddress(pg.IPv4Address(ip,netmask))
        lan.addInterface(iface)
    request.addResource(node)

if lan:
    request.addResource(lan)

pc.printRequestRSpec(request)
