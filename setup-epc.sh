#!/bin/sh

set -x

export SRC=`dirname $0`
cd $SRC
. $SRC/setup-lib.sh

if [ -f $OURDIR/setup-epc-done ]; then
    echo "setup-epc already ran; not running again"
    exit 0
fi

cd $OURDIR

maybe_install_packages mongodb
service_enable mongodb
service_start mongodb

maybe_install_packages \
    autoconf libtool gcc pkg-config \
    git flex bison libsctp-dev libgnutls28-dev libgcrypt-dev \
    libssl-dev libidn11-dev libmongoc-dev libbson-dev libyaml-dev

curl -sL https://deb.nodesource.com/setup_10.x | $SUDO -E bash -
maybe_install_packages nodejs

PREFIX=/local/root
mkdir -p $PREFIX

git clone https://github.com/nextepc/nextepc
cd nextepc
autoreconf -iv
./configure --prefix=$PREFIX
make -j `nproc`
make install

cd $OURDIR/nextepc/webui
$SUDO npm install

$SUDO ip tuntap add pgwtun mode tun
$SUDO ip addr add 192.168.0.1/24 dev pgwtun
$SUDO ip link set up dev pgwtun
$SUDO iptables -t nat -A POSTROUTING -o ${EXTERNAL_NETWORK_INTERFACE} \
    -j MASQUERADE

cat <<EOF >$PREFIX/etc/nextepc/nextepc.conf
db_uri: mongodb://localhost/nextepc
logger:
    file: $PREFIX/var/log/nextepc/nextepc.log
    trace:
        app: 1
        s1ap: 1
        nas: 1
        diameter: 1
        gtpv2: 1
        gtp: 1
parameter:
    no_ipv6: true
mme:
    freeDiameter: mme.conf
    s1ap:
      addr: $EPC_IP
    gtpc:
      addr: 127.0.0.1
    gummei:
      plmn_id:
        mcc: 998
        mnc: 98
      mme_gid: 2
      mme_code: 1
    tai:
      plmn_id:
        mcc: 998
        mnc: 98
      tac: 1
    security:
        integrity_order : [ EIA1, EIA2, EIA0 ]
        ciphering_order : [ EEA0, EEA1, EEA2 ]
    network_name:
        full: NextEPC
hss:
    freeDiameter: hss.conf
sgw:
    gtpc:
      addr: 127.0.0.2
    gtpu:
      addr: $EPC_IP
pgw:
    freeDiameter: pgw.conf
    gtpc:
      addr:
        - 127.0.0.3
        - ::1
    gtpu:
      - addr: 127.0.0.3
      - addr: ::1
    ue_pool:
      - addr: 192.168.0.1/24
    dns:
      - 8.8.8.8
      - 8.8.4.4
      - 2001:4860:4860::8888
      - 2001:4860:4860::8844
pcrf:
    freeDiameter: pcrf.conf
EOF

touch $OURDIR/setup-epc-done

exit 0
