#!/bin/sh

set -x

export SRC=`dirname $0`
cd $SRC
. $SRC/setup-lib.sh

if [ -f $OURDIR/setup-ric-done ]; then
    echo "setup-ric already ran; not running again"
    exit 0
fi

if [ -z "$UPDATEIMAGES" ]; then
    UPDATEIMAGES=0
fi
DEMOBASE=ubuntu:18.04

# Install Docker and git.
dpkg-query -l docker.io >/dev/null 2>&1
if [ ! $? -eq 0 ]; then
    $SUDO apt-get -y update
    $SUDO apt-get install -y docker.io git
fi

# Give experiment creator ability to talk to Docker daemon sans sudo.
# This doesn't help us in this script, of course.
SWAPPER=`cat /var/emulab/boot/swapper`
groups $SWAPPER | grep -q docker
if [ ! $? -eq 0 ]; then
    $SUDO usermod -a -G docker $SWAPPER
fi

# Pull a useful generic Ubuntu image.
$SUDO docker image inspect $DEMOBASE >/dev/null 2>&1
if [ ! $? -eq 0 ]; then
    $SUDO docker pull $DEMOBASE
fi

# Login to o-ran docker registry server (both staging and release) so
# that Dockerfile base images can be pulled.
$SUDO docker login -u docker -p docker https://nexus3.o-ran-sc.org:10004
$SUDO docker login -u docker -p docker https://nexus3.o-ran-sc.org:10002
$SUDO chown -R $SWAPPER ~/.docker

# Build e2sim.
$SUDO docker image inspect e2sim:latest >/dev/null 2>&1
if [ ! $? -eq 0 -o $UPDATEIMAGES = "1" ]; then
    if [ ! -e test ]; then
	git clone https://gerrit.o-ran-sc.org/r/it/test.git
    fi
    cd test/simulators/e2sim/
    if [ $UPDATEIMAGES = "1" ]; then
	git pull
    fi
    tagvers=`git log --pretty=format:"%h" -n 1`
    $SUDO docker image inspect e2sim:$tagvers >/dev/null 2>&1
    if [ ! $? -eq 0 ]; then
	$SUDO docker build -f docker/Dockerfile -t e2sim:$tagvers .
    fi
    $SUDO docker tag e2sim:$tagvers e2sim:latest
    cd ../../..
fi

# Build e2(term).
$SUDO docker image inspect e2term:latest >/dev/null 2>&1
if [ ! $? -eq 0 -o $UPDATEIMAGES = "1" ]; then
    if [ ! -e e2 ]; then
	git clone https://gerrit.o-ran-sc.org/r/ric-plt/e2
    fi
    cd e2
    git checkout -b $RICBRANCH origin/$RICBRANCH
    if [ $RICBRANCH = "r2" -o $RICBRANCH = "Amber" ]; then
	# The r2 and Amber branch builds use an orphaned image.
	$SUDO docker pull nexus3.o-ran-sc.org:10004/bldr-ubuntu16-c-go:3-u16.04-nng
	$SUDO docker tag nexus3.o-ran-sc.org:10004/bldr-ubuntu16-c-go:3-u16.04-nng \
	    nexus3.o-ran-sc.org:10004/bldr-ubuntu16-c-go:2-u16.04-nng
    elif [ $RICBRANCH = "bronze" ]; then
	# The bronze branch builds use an orphaned image.
	$SUDO docker pull nexus3.o-ran-sc.org:10004/o-ran-sc/bldr-ubuntu18-c-go:9-u18.04
	$SUDO docker tag nexus3.o-ran-sc.org:10004/o-ran-sc/bldr-ubuntu18-c-go:9-u18.04 \
	    nexus3.o-ran-sc.org:10004/o-ran-sc/bldr-ubuntu18-c-go:8-u18.04
    fi
    cd RIC-E2-TERMINATION
    if [ $UPDATEIMAGES = "1" ]; then
	git pull
    fi
    tagvers=`git log --pretty=format:"%h" -n 1`
    $SUDO docker image inspect e2term:$tagvers >/dev/null 2>&1
    if [ ! $? -eq 0 ]; then
	$SUDO docker build -f Dockerfile -t e2term:$tagvers .
    fi
    $SUDO docker tag e2term:$tagvers e2term:latest
    cd ../..
fi

# Build e2mgr.
$SUDO docker image inspect e2mgr:latest >/dev/null 2>&1
if [ ! $? -eq 0 -o $UPDATEIMAGES = "1" ]; then
    if [ ! -e e2mgr ]; then
	git clone https://gerrit.o-ran-sc.org/r/ric-plt/e2mgr
    fi
    cd e2mgr
    git checkout -b $RICBRANCH origin/$RICBRANCH
    if [ $RICBRANCH = "Amber" ]; then
	# The Amber branch build uses an orphaned image.
	$SUDO docker pull nexus3.o-ran-sc.org:10004/bldr-ubuntu16-c-go:3-u16.04-nng
	$SUDO docker tag nexus3.o-ran-sc.org:10004/bldr-ubuntu16-c-go:3-u16.04-nng \
	    nexus3.o-ran-sc.org:10004/bldr-ubuntu16-c-go:2-u16.04-nng
    fi
    if [ $UPDATEIMAGES = "1" ]; then
	git pull
    fi
    tagvers=`git log --pretty=format:"%h" -n 1`
    $SUDO docker image inspect e2mgr:$tagvers >/dev/null 2>&1
    if [ ! $? -eq 0 ]; then
	cd E2Manager
	$SUDO docker build -f Dockerfile -t e2mgr:$tagvers .
    fi
    $SUDO docker tag e2mgr:$tagvers e2mgr:latest
    cd ../..
fi

if [ "$RICBRANCH" = "master" -o "$RICBRANCH" = "bronze" ]; then
    $SUDO docker image inspect e2rtmansim:latest >/dev/null 2>&1
    if [ ! $? -eq 0 -o $UPDATEIMAGES = "1" ]; then
	$SUDO docker pull nexus3.o-ran-sc.org:10004/bldr-ubuntu16-c-go:3-u16.04-nng
	$SUDO docker tag nexus3.o-ran-sc.org:10004/bldr-ubuntu16-c-go:3-u16.04-nng \
	    nexus3.o-ran-sc.org:10004/bldr-ubuntu16-c-go:2-u16.04-nng
	cd e2mgr/tools/RoutingManagerSimulator
	tagvers=`git log --pretty=format:"%h" -n 1`
	$SUDO docker image inspect e2rtmansim:$tagvers >/dev/null 2>&1
	if [ ! $? -eq 0 ]; then
	    $SUDO docker build -f Dockerfile -t e2rtmansim:$tagvers .
	fi
	$SUDO docker tag e2rtmansim:$tagvers e2rtmansim:latest
	cd ../../..
    fi
fi

# Build dbaas.
$SUDO docker image inspect dbaas:latest >/dev/null 2>&1
if [ ! $? -eq 0 ]; then
    if [ ! -e dbaas ]; then
	git clone https://gerrit.o-ran-sc.org/r/ric-plt/dbaas
    fi
    #if [ "$RICBRANCH" = "bronze" ]; then
    #   docker pull nexus3.o-ran-sc.org:10004/o-ran-sc/bldr-alpine3-rmr:4.1.2
    #	docker tag nexus3.o-ran-sc.org:10004/o-ran-sc/bldr-alpine3-rmr:4.1.2 \
    #	    nexus3.o-ran-sc.org:10004/o-ran-sc/bldr-alpine3:12-a3.11
    #fi
    cd dbaas
    $SUDO docker build -f docker/Dockerfile.redis -t dbaas:latest .
    cd ..
fi

# Build a simple Ubuntu image with ping and curl for testing.
$SUDO docker image inspect ric-demo:latest >/dev/null 2>&1
if [ ! $? -eq 0 ]; then
    mkdir -p ric-demo-image
    cd ric-demo-image
    cat <<EOF >Dockerfile
FROM $DEMOBASE

RUN apt-get -y update \\
  && apt-get -y install iputils-ping iproute2 curl lksctp-tools \\
  && apt-get -y clean all \\
  && rm -rf /var/lib/apt/lists/*

EOF
    $SUDO docker build -f Dockerfile -t ric-demo:latest .
    cd ..
fi

# Create a private network for the RIC and demo containers.
$SUDO docker network inspect ric >/dev/null 2>&1
if [ ! $? -eq 0 ]; then
    $SUDO brctl addbr brric
    $SUDO docker network create --subnet=$RIC_SUBNET -d bridge --attachable \
	-o com.docker.network.bridge.name=brric ric
fi

# Move the expt net device into the docker bridge, if we are simulating
# 4G components too.
if [ $DORAN -eq 1 -o $DOEPC -eq 1 ]; then
    EXPMAC=`cat /var/emulab/boot/tmcc/ifconfig | sed -ne "s/^.*INET=10.0.2.1.* MAC=\([0-9a-fA-F:]*\) .*$/\1/p"`
    EXPIF=`/usr/local/etc/emulab/findif $EXPMAC`
    $SUDO brctl addif brric $EXPIF
fi

# Create a route info file to tell the containers where to send various
# messages.  We will mount this into the various containers in the right
# place.
ROUTERFILE=`pwd`/router.txt
if [ ! -e $ROUTERFILE ]; then
    cat <<EOF >$ROUTERFILE
newrt|start
rte|10020|$E2MGR_IP:3801
rte|10060|$E2TERM_IP:38000
rte|10061|$E2MGR_IP:3801
rte|10062|$E2MGR_IP:3801
rte|10070|$E2MGR_IP:3801
rte|10071|$E2MGR_IP:3801
rte|10080|$E2MGR_IP:3801
rte|10081|$E2TERM_IP:38000
rte|10082|$E2TERM_IP:38000
rte|10360|$E2TERM_IP:38000
rte|10361|$E2MGR_IP:3801
rte|10362|$E2MGR_IP:3801
rte|10370|$E2MGR_IP:3801
rte|10371|$E2TERM_IP:38000
rte|10372|$E2TERM_IP:38000
rte|1080|$E2MGR_IP:3801
rte|1090|$E2TERM_IP:38000
rte|1100|$E2MGR_IP:3801
rte|12010|$E2MGR_IP:38010
rte|1101|$E2TERM_IP:38000
rte|12002|$E2TERM_IP:38000
rte|12003|$E2TERM_IP:38000
rte|10091|$E2MGR_IP:4801
rte|10092|$E2MGR_IP:4801
rte|1101|$E2TERM_IP:38000
rte|1102|$E2MGR_IP:3801
rte|12001|$E2MGR_IP:3801
newrt|end
EOF
# Not sure what this route/msg is for?
# rte|12010|10.0.2.15:38010
fi

remove_container() {
    $SUDO docker inspect $1 >/dev/null 2>&1
    if [ $? -eq 0 ]; then
	$SUDO docker kill $1
	$SUDO docker rm $1
    fi
}

# Create the various containers.  Kill and remove them if they exist.
remove_container db
$SUDO docker run -d --network ric --ip $DBAAS_IP --name db dbaas:latest

if [ "$RICBRANCH" = "master" -o "$RICBRANCH" = "bronze" ]; then
    remove_container e2rtmansim
    $SUDO docker run -d -it --network ric --ip $E2RTMANSIM_IP \
	--name e2rtmansim e2rtmansim:latest
fi

remove_container e2mgr
$SUDO docker run -d -it --network ric --ip $E2MGR_IP -e RIC_ID=7b0000-000000/18 \
    -e DBAAS_PORT_6379_TCP_ADDR=$DBAAS_IP -e DBAAS_PORT_6379_TCP_PORT="6379" \
    -e DBAAS_SERVICE_HOST=$DBAAS_IP -e DBAAS_SERCE_PORT="6379" \
    --mount type=bind,source=$ROUTERFILE,destination=/opt/E2Manager/router.txt,ro \
    --name e2mgr e2mgr:latest

remove_container e2term
if [ "$RICBRANCH" = "master" -o "$RICBRANCH" = "bronze" ]; then
    E2TERMCONFFILE=`pwd`/e2term_config.conf
    if [ ! -e $E2TERMCONFFILE ]; then
	cat <<EOF >$E2TERMCONFFILE
nano=38000
loglevel=debug
volume=log
#the key name of the environment holds the local ip address
#ip address of the E2T in the RMR
local-ip=$E2TERM_IP
#trace is start, stop
trace=start
external-fqdn=e2t.com
#put pointer to the key that point to pod name
pod_name=E2TERM_POD_NAME
sctp-port=36422
EOF
    fi
    E2TERM_CONFIG_BIND="--mount type=bind,source=$E2TERMCONFFILE,destination=/opt/e2/config/config.conf,ro"
fi
$SUDO docker run -d --network=ric --ip $E2TERM_IP --name e2term \
    --mount type=bind,source=$ROUTERFILE,destination=/opt/e2/dockerRouter.txt,ro \
    $E2TERM_CONFIG_BIND \
    e2term:latest

remove_container e2sim
$SUDO docker run -d --network=ric --ip $E2SIM_IP --name e2sim \
    e2sim:latest /home/e2sim/build/e2sim $E2SIM_IP 1234

touch $OURDIR/setup-ric-done

exit 0

#
# How to run some demos (note, these assume your e2sim enodeb is on
# 10.0.2.100, as specified above).
#
# 1. Open 5 ssh connections to your node.  Run the following commands in each:
#  * docker logs -f e2term
#  * docker logs -f e2mgr
#  * docker logs -f db
#  * docker logs -f e2sim
#
# (These commands allow you to view the stdout/stderr of PID 1 in the
# container; e.g., the e2 components or the redis db.  The 5th shell
# will be for manually typing API requests to the e2mgr via curl.)
#
# 2. Spawn a temporary demo container attached to the `ric` network in
#    your 5th shell:
#
#     docker run --rm -it --network ric --name demo ric-demo:latest /bin/bash
#
# 3. Connect an LTE enodeb via X2-SETUP-REQUEST (run all these commands
#    in your 5th shell)
#
#   - Send X2 Setup Request:
#
#     curl -X POST http://e2mgr:3800/v1/nodeb/x2-setup -H 'Content-Type: application/json' -d '{"ranIp":"10.0.2.100","ranPort":1234,"ranName":"test1"}'
#
#   - List the connected RAN nodes:
#
#     curl -X GET http://e2mgr:3800/v1/nodeb/ids
#
#   - Show the `test1` RAN node:
#
#     curl -X GET http://e2mgr:3800/v1/nodeb/test1
#
#   - Tell the e2mgr to shutdown the simulated enodeb (removes e2 state
#     for the e2sim RAN node, and breaks the connection to the e2sim,
#     which ends its process, and thus its container stop).
#
#     curl -X PUT http://e2mgr:3800/v1/nodeb/shutdown
#
# 4. Restart the e2sim container and try the gNB variant on this:
#
#   - (in e2sim shell above) docker restart e2sim && docker logs -f e2sim
#
#   - (in demo shell spawned in step 2) Send an endc setup message:
#
#     curl -X POST http://e2mgr:3800/v1/nodeb/endc-setup -H 'Content-Type: application/json' -d '{"ranIp":"10.0.2.100","ranPort":1234,"ranName":"test1"}'
#
#   - (Use other messages above.)
#
